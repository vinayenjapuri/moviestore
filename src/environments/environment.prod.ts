export const environment = {
  production: true,
  webServiceUrl: 'https://cinematics-dump-service.onrender.com',
  movies_aggregate: "movies-aggregate",
  series_aggregate: 'series-aggregate',
  animes_aggregate: 'animes-aggregate',
  mangas_aggregate: 'mangas-aggregate',
};
