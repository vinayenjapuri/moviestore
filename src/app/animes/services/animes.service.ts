import { Injectable } from '@angular/core';
import { CinematicDumpAggregate, ItemAggregate } from '../../shared/models/cinematic-dump.model';
import { HttpClient } from '@angular/common/http';
import { Observable, catchError, map } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AnimesService {

  // private _animesAggregate:AnimesAggregate[] = [];
  //
  // public set setAnimesAggregate(movies:AnimesAggregate[]) {
  //   this._animesAggregate = movies;
  // }
  //
  // public get getAnimesAggregate():AnimesAggregate[] {
  //   return this._animesAggregate;
  // }

  constructor(private httpClient: HttpClient) { }

  getAggregate(): Observable<CinematicDumpAggregate[]> {
    return this.httpClient.get<ItemAggregate[]>(`${environment.webServiceUrl}/${environment.animes_aggregate}`).pipe(
      map(items => {
        const cinematicDumpAggregate: CinematicDumpAggregate[] = [];
        items.forEach(item => {
          cinematicDumpAggregate.push(new CinematicDumpAggregate(item));
        });
        return cinematicDumpAggregate;
      }), catchError((err, caught) => {
        return [];
      })
    );
  }
}
