import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnimeRootComponent } from './anime-root/anime-root.component';

const routes: Routes = [
  {
    path: '',
    component: AnimeRootComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnimesRoutingModule { }
