import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimeRootComponent } from './anime-root.component';

describe('AnimeRootComponent', () => {
  let component: AnimeRootComponent;
  let fixture: ComponentFixture<AnimeRootComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AnimeRootComponent]
    });
    fixture = TestBed.createComponent(AnimeRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
