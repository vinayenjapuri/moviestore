import { AnimesService } from './../services/animes.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { CinematicDumpState } from 'src/app/cinematic-dump-store/cd-state';
import {AbstractRootControl} from "../../shared/abstract-classes/abstract-root-control";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'ms-anime-root',
  templateUrl: './anime-root.component.html',
  styleUrls: ['./anime-root.component.scss']
})
export class AnimeRootComponent extends AbstractRootControl implements OnInit, OnDestroy {

  constructor(private animesService: AnimesService, private movieStore: Store<CinematicDumpState>, private activated: ActivatedRoute) {
    super(movieStore,  animesService, activated);
  }

  ngOnDestroy(): void {
    this.dataSubscription?.unsubscribe();
    this.cinematicDumpAggregateSubscription?.unsubscribe();
    this.queryParamsSubscription?.unsubscribe();
    this.currentModeSubscription$?.unsubscribe();
    this.mobileResolutionSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.registerConfigurationImplementation().then(() => {

    });
  }
}
