import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnimesRoutingModule } from './animes-routing.module';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { cinematicDumpReducer } from '../cinematic-dump-store/cd-reducers';
import { AnimeRootComponent } from './anime-root/anime-root.component';
import { ItemDetailedContentSvg } from '../shared/components/item-detailed-content/item-detailed-content.svg';
import {StructuralLoadingSvg} from "../shared/components/structural-loading/structural-loading.svg";
import {ItemsContainerComponent} from "../shared/components/items-container/items-container.component";
import {LoadingComponent} from "../shared/components/loading/loading.component";



@NgModule({
  declarations: [
    AnimeRootComponent
  ],
  imports: [
    CommonModule,
    AnimesRoutingModule,
    SharedModule,
    StoreModule.forFeature('cinematic-dumpState', cinematicDumpReducer),
    ItemDetailedContentSvg,
    StructuralLoadingSvg,
    ItemsContainerComponent,
    LoadingComponent
  ]
})
export class AnimesModule { }
