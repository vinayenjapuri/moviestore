import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import { CinematicDumpState } from './cinematic-dump-store/cd-state';
import { Store } from '@ngrx/store';
import {setInnerWidth, setMobileResolution, setMode} from './cinematic-dump-store/cd-action';
import {Subscription} from "rxjs";
import {bodyConfigSetUp} from "./shared/abstract-classes/app-control";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  private windowResizeSubscription?: Subscription;

  public isMobileResolution: boolean = false;

  constructor(private store: Store<CinematicDumpState>) {
  }
  ngOnInit(): void {
    this.store.dispatch(setMode(bodyConfigSetUp()));
    this.setWindowResize(window.innerWidth);
  }

  @HostListener('window:resize', ['$event']) onResize(event: any) {
    this.setWindowResize(event.target.innerWidth);
  }


  private setWindowResize(width: number) {
    this.isMobileResolution = width < 947;
    this.store.dispatch(setMobileResolution(this.isMobileResolution));
    this.store.dispatch(setInnerWidth(width));
  }

  ngOnDestroy(): void {
    this.windowResizeSubscription?.unsubscribe();
  }
}
