import { createFeatureSelector, createSelector } from "@ngrx/store";
import { CinematicDumpState } from './cd-state';

export const selectCDumpState = createFeatureSelector<CinematicDumpState>('cinematic-dumpState');

export const getCurrentMode = createSelector(selectCDumpState, (state: CinematicDumpState) => state?.mode);

export const getMoviesAggregate = createSelector(selectCDumpState, (state: CinematicDumpState) => state?.moviesAggregate);
export const getAnimesAggregate = createSelector(selectCDumpState, (state: CinematicDumpState) => state?.animesAggregate);
export const getMangasAggregate = createSelector(selectCDumpState, (state: CinematicDumpState) => state?.mangasAggregate);
export const getSeriesAggregate = createSelector(selectCDumpState, (state: CinematicDumpState) => state?.seriesAggregate);
export const getSelectedAggregate = createSelector(selectCDumpState, (state: CinematicDumpState) => state?.selectedAggregate);

export const getMobileResolution = createSelector(selectCDumpState, (state: CinematicDumpState) => state?.mobileResolution);
export const getInnerWidth = createSelector(selectCDumpState, (state: CinematicDumpState) => state?.innerWidth);

export const getSelectedGenres = createSelector(selectCDumpState, (state: CinematicDumpState) => state?.selectedGenres)
