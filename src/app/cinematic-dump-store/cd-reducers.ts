import { createReducer, on } from '@ngrx/store';
import { CinematicDumpState } from './cd-state';
import {
  setAnimesAggregate, setInnerWidth, setMangasAggregate,
  setMobileResolution,
  setMode,
  setMoviesAggregate,
  setSelectedAggregate, setSelectedGenreOnType,
  setSeriesAggregate
} from './cd-action';


export const CDumpState: CinematicDumpState = {
  mode: undefined,
  moviesAggregate: [],
  animesAggregate: [],
  mangasAggregate: [],
  seriesAggregate: [],
  selectedAggregate: undefined,
  mobileResolution: false,
  innerWidth: 0,
  selectedGenres: undefined
}

export const cinematicDumpReducer = createReducer(
  CDumpState,
  on(setMode, (state, {mode}) => ({
      ...state,
      mode: mode
  })),
  on(setMoviesAggregate, (state, {moviesAggregate}) => ({
    ...state,
    moviesAggregate: moviesAggregate
  })),
  on(setSeriesAggregate, (state, {seriesAggregate}) => ({
    ...state,
    seriesAggregate: seriesAggregate
  })),
  on(setAnimesAggregate, (state, {animesAggregate}) => ({
    ...state,
    moviesAggregate: animesAggregate
  })),
  on(setMangasAggregate, (state, {mangasAggregate}) => ({
    ...state,
    moviesAggregate: mangasAggregate
  })),
  on(setSelectedAggregate, (state, {selectedAggregate}) => ({
    ...state,
    selectedAggregate: selectedAggregate
  })),
  on(setMobileResolution, (state, {isMobileResolution}) => ({
    ...state,
    mobileResolution: isMobileResolution
  })),
  on(setInnerWidth, (state, {innerWidth}) => ({
    ...state,
    innerWidth: innerWidth
  })),
  on(setSelectedGenreOnType, (state, {selectedGenres}) => ({
    ...state,
    selectedGenres: selectedGenres
  }))
)
