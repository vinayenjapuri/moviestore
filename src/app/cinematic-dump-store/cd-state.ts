import { CINEMATIC_DUMP_MODE_TYPES } from "../shared/enums/cinematic-dump-mode-types";
import {
  ItemAggregate
} from "../shared/models/cinematic-dump.model";

export interface CinematicDumpState {
  mode?: CINEMATIC_DUMP_MODE_TYPES;
  moviesAggregate?: ItemAggregate[];
  animesAggregate?: ItemAggregate[];
  mangasAggregate?: ItemAggregate[];
  seriesAggregate?: ItemAggregate[];
  selectedAggregate?: ItemAggregate;
  mobileResolution?: boolean;
  innerWidth?: number;
  selectedGenres?: SelectedGenres
}

export interface SelectedGenres {
  type?: string;
  genre?: string;
}
