import { createAction } from "@ngrx/store";
import { CINEMATIC_DUMP_MODE_TYPES } from "../shared/enums/cinematic-dump-mode-types";
import { ItemAggregate } from '../shared/models/cinematic-dump.model';
import {SelectedGenres} from "./cd-state";

export const setMode = createAction('setMode', (mode?: CINEMATIC_DUMP_MODE_TYPES) => ({mode}));
export const setMoviesAggregate = createAction('setMoviesAggregate', (moviesAggregate?: ItemAggregate[]) => ({moviesAggregate}));
export const setSeriesAggregate = createAction('setSeriesAggregate', (seriesAggregate?: ItemAggregate[]) => ({seriesAggregate}));
export const setAnimesAggregate = createAction('setAnimesAggregate', (animesAggregate?: ItemAggregate[]) => ({animesAggregate}));
export const setMangasAggregate = createAction('setMangasAggregate', (mangasAggregate?: ItemAggregate[]) => ({mangasAggregate}));
export const setSelectedAggregate = createAction('setSelectedAggregate', (selectedAggregate?: ItemAggregate) => ({selectedAggregate}));

export const setMobileResolution = createAction('setMobileResolution', (isMobileResolution?: boolean) => ({isMobileResolution}));
export const setInnerWidth = createAction('setInnerWidth', (innerWidth?: number) => ({innerWidth}));

export const setSelectedGenreOnType = createAction('setSelectedGenreOnType', (selectedGenres?: SelectedGenres) => ({selectedGenres}));

