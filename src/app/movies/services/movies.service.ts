import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable, map, catchError} from 'rxjs';
import {CinematicDumpAggregate, ItemAggregate} from '../../shared/models/cinematic-dump.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  // private _moviesAggregate: MoviesAggregate[] = [];
  //
  // public set setMoviesAggregate(movies: MoviesAggregate[]) {
  //   this._moviesAggregate = movies;
  // }
  //
  // public get getMoviesAggregate(): MoviesAggregate[] {
  //   return this._moviesAggregate;
  // }

  constructor(private httpClient: HttpClient) { }

  getAggregate(): Observable<CinematicDumpAggregate[]> {
    return this.httpClient.get<ItemAggregate[]>(`${environment.webServiceUrl}/${environment.movies_aggregate}`).pipe(
      map(items => {
        const cinematicDumpAggregate: CinematicDumpAggregate[] = [];
        items.forEach(item => {
          cinematicDumpAggregate.push(new CinematicDumpAggregate(item));
        });
        return cinematicDumpAggregate;
      }), catchError((err, caught) => {
        return [];
      })
    );
  }
}
