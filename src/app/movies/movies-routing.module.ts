import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MsMovieRootComponent} from "./ms-movie-root/ms-movie-root.component";

const routes: Routes = [
  {path: '', component: MsMovieRootComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MoviesRoutingModule { }
