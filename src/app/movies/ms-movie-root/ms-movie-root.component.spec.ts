import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MsMovieRootComponent } from './ms-movie-root.component';

describe('MsMovieRootComponent', () => {
  let component: MsMovieRootComponent;
  let fixture: ComponentFixture<MsMovieRootComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MsMovieRootComponent]
    });
    fixture = TestBed.createComponent(MsMovieRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
