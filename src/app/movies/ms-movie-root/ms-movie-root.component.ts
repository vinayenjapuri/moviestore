import {Component, OnDestroy, OnInit} from '@angular/core';
import {MoviesService} from "../services/movies.service";
import {Store} from "@ngrx/store";
import {CinematicDumpState} from "../../cinematic-dump-store/cd-state";
import {AbstractRootControl} from "../../shared/abstract-classes/abstract-root-control";
import {ActivatedRoute} from "@angular/router";
import {getMobileResolution} from "../../cinematic-dump-store/cd-selectors";

@Component({
  selector: 'ms-ms-movie-root',
  templateUrl: './ms-movie-root.component.html',
  styleUrls: ['./ms-movie-root.component.scss']
})
export class MsMovieRootComponent extends AbstractRootControl implements OnInit, OnDestroy {
  constructor(private moviesService: MoviesService, private movieStore: Store<CinematicDumpState>, private activated: ActivatedRoute) {
    super(movieStore, moviesService, activated);
  }

  ngOnDestroy(): void {
    this.dataSubscription?.unsubscribe();
    this.cinematicDumpAggregateSubscription?.unsubscribe();
    this.queryParamsSubscription?.unsubscribe();
    this.mobileResolutionSubscription?.unsubscribe();
    this.currentModeSubscription$?.unsubscribe();
  }

  ngOnInit(): void {
    this.subscribeToMobileResolution();
    this.isLoading = true;
    this.registerConfigurationImplementation().then(() => {
    });
  }

  private subscribeToMobileResolution(): void {
    this.mobileResolutionSubscription = this.movieStore.select(getMobileResolution).subscribe(isMobileResolution => {
      console.log(isMobileResolution);
      this.isMobileResolution = isMobileResolution!;
    })
  }

}
