import {CinematicDumpAggregate, ItemAggregate} from "../shared/models/cinematic-dump.model";
import {Store} from "@ngrx/store";
import {CinematicDumpState} from "../cinematic-dump-store/cd-state";
import {
  getAnimesAggregate,
  getCurrentMode,
  getMangasAggregate,
  getMoviesAggregate,
  getSeriesAggregate
} from "../cinematic-dump-store/cd-selectors";
import {Subscription} from "rxjs";
import {forEach} from 'lodash';
import {FormControl} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {setSelectedAggregate} from '../cinematic-dump-store/cd-action';
import {ActivatedRoute, Router} from "@angular/router";
import {CINEMATIC_DUMP_MODE_TYPES} from "../shared/enums/cinematic-dump-mode-types";


interface OptionCategory {
  key: string;
  value: string;
  isSelected: boolean;
  counts: number;
}
export class AbstractItemControl {

  public filteredList: CinematicDumpAggregate[] = [];

  private _itemAggregate?: ItemAggregate[];

  public control = new FormControl('');

  public isParallelLoading: boolean = false;

  public genreChipList: OptionCategory[] = [
    {key: "Comedy", value: "Comedy", isSelected: false, counts: 0},
    {key: "Romance", value: "Romance", isSelected: false, counts: 0},
    {key: "Adventure", value: "Adventure", isSelected: false, counts: 0},
    {key: "Thriller", value: "Thriller", isSelected: false, counts: 0},
    {key: "Sci-Fi", value: "Sci-Fi", isSelected: false, counts: 0},
    {key: "Horror", value: "Horror", isSelected: false, counts: 0},
    {key: "Mystery", value: "Mystery", isSelected: false, counts: 0},
    {key: "Drama", value: "Drama", isSelected: false, counts: 0},
    {key: "Animated", value: "Animated", isSelected: false, counts: 0},
    {key: "Action", value: "Action", isSelected: false, counts: 0},
    {key: "Crime", value: "Crime", isSelected: false, counts: 0},
    {key: "Erotic", value: "Erotic", isSelected: false, counts: 0},
    {key: "Fantasy", value: "Fantasy", isSelected: false, counts: 0},
    {key: "Disaster", value: "Disaster", isSelected: false, counts: 0},
    {key: "Family", value: "Family", isSelected: false, counts: 0},
    {key: "Suspense", value: "Suspense", isSelected: false, counts: 0},
    {key: "War", value: "War", isSelected: false, counts: 0},
    {key: "Documentary", value: "Documentary", isSelected: false, counts: 0},
    {key: "Supernatural", value: "Supernatural", isSelected: false, counts: 0},
    {key: "Legal", value: "Legal", isSelected: false, counts: 0},
    {key: "Satire", value: "Satire", isSelected: false, counts: 0},
    {key: "Political", value: "Political", isSelected: false, counts: 0},
    {key: "Noir", value: "Noir", isSelected: false, counts: 0},
    {key: "Independent", value: "Independent", isSelected: false, counts: 0},
    {key: "Martial Arts", value: "Martial Arts", isSelected: false, counts: 0},
  ];
  public yearsChipList: OptionCategory[] = [
    {key: "1990", value: "1990", isSelected: false, counts: 0},
    {key: "1991", value: "1991", isSelected: false, counts: 0},
    {key: "1992", value: "1992", isSelected: false, counts: 0},
    {key: "1993", value: "1993", isSelected: false, counts: 0},
    {key: "1994", value: "1994", isSelected: false, counts: 0},
    {key: "1995", value: "1995", isSelected: false, counts: 0},
    {key: "1996", value: "1996", isSelected: false, counts: 0},
    {key: "1997", value: "1997", isSelected: false, counts: 0},
    {key: "1998", value: "1998", isSelected: false, counts: 0},
    {key: "1999", value: "1999", isSelected: false, counts: 0},
    {key: "2000", value: "2000", isSelected: false, counts: 0},
    {key: "2001", value: "2001", isSelected: false, counts: 0},
    {key: "2002", value: "2002", isSelected: false, counts: 0},
    {key: "2003", value: "2003", isSelected: false, counts: 0},
    {key: "2004", value: "2004", isSelected: false, counts: 0},
    {key: "2006", value: "2006", isSelected: false, counts: 0},
    {key: "2005", value: "2005", isSelected: false, counts: 0},
    {key: "2007", value: "2007", isSelected: false, counts: 0},
    {key: "2008", value: "2008", isSelected: false, counts: 0},
    {key: "2009", value: "2009", isSelected: false, counts: 0},
    {key: "2010", value: "2010", isSelected: false, counts: 0},
    {key: "2011", value: "2011", isSelected: false, counts: 0},
    {key: "2012", value: "2012", isSelected: false, counts: 0},
    {key: "2013", value: "2013", isSelected: false, counts: 0},
    {key: "2014", value: "2014", isSelected: false, counts: 0},
    {key: "2015", value: "2015", isSelected: false, counts: 0},
    {key: "2016", value: "2016", isSelected: false, counts: 0},
    {key: "2017", value: "2017", isSelected: false, counts: 0},
    {key: "2018", value: "2018", isSelected: false, counts: 0},
    {key: "2019", value: "2019", isSelected: false, counts: 0},
    {key: "2020", value: "2020", isSelected: false, counts: 0},
    {key: "2021", value: "2021", isSelected: false, counts: 0},
    {key: "2022", value: "2022", isSelected: false, counts: 0},
    {key: "2023", value: "2023", isSelected: false, counts: 0},
  ];

  public currentModeSubscription$?: Subscription;

  public currentMode?: CINEMATIC_DUMP_MODE_TYPES;
  public valuChangeSubscription$?: Subscription;

  public set itemAggregate(items: ItemAggregate[]) {
    this._itemAggregate = items ?? [];
  }

  public get itemAggregate(): ItemAggregate[] {
    return this._itemAggregate ?? [];
  }

  public _itemDumpAggregate?: CinematicDumpAggregate[];

  public selectedItemAggregate?: ItemAggregate;


  set itemDumpAggregate(dumpAggregates: CinematicDumpAggregate[]) {
    this._itemDumpAggregate = dumpAggregates ?? [];
  }

  get itemDumpAggregate(): CinematicDumpAggregate[] {
    return this._itemDumpAggregate ?? [];
  }

  public itemSubscription?: Subscription;

  public context_type?: "panel" | "document";

  public ref?: any;

  constructor(private itemStore: Store<CinematicDumpState>,
              private matSnackBar: MatSnackBar,
              private routes: Router,
              private activatedRoutes: ActivatedRoute) {
    let setup =  async( ) => {
      await this.subscribeToCurrentMode();
      this.subscribeToItemAggregate();
    }
    setup().then();
  }


  private subscribeToItemAggregate() {
    this.isParallelLoading = true;
    switch (this.currentMode) {
      case CINEMATIC_DUMP_MODE_TYPES.MOVIES:
        this.itemSubscription = this.itemStore.select(getMoviesAggregate).subscribe(items => {
          this.itemAggregate = items ?? [];
          this.filterChipsCountSetUp();
          this.isParallelLoading = false;
        });
        break;
      case CINEMATIC_DUMP_MODE_TYPES.ANIMES:
        this.itemSubscription = this.itemStore.select(getAnimesAggregate).subscribe(items => {
          this.itemAggregate = items ?? [];
          this.filterChipsCountSetUp();
          this.isParallelLoading = false;
        });
        break;
      case CINEMATIC_DUMP_MODE_TYPES.MANGAS:
        this.itemSubscription = this.itemStore.select(getMangasAggregate).subscribe(items => {
          this.itemAggregate = items ?? [];
          this.filterChipsCountSetUp();
          this.isParallelLoading = false;
        });
        break;
      case CINEMATIC_DUMP_MODE_TYPES.SERIES:
        this.itemSubscription = this.itemStore.select(getSeriesAggregate).subscribe(items => {
          this.itemAggregate = items ?? [];
          this.filterChipsCountSetUp();
          this.isParallelLoading = false;
        });
        break;
      default:
        this.itemAggregate = [];
        this.filterChipsCountSetUp();
        this.isParallelLoading = false;
       break;
    }
  }

  public setSelectedItem(highlightMovie: ItemAggregate, highlight: boolean): void {
    for(const item of this.itemDumpAggregate) {
      if (item.data?.title === highlightMovie.title && highlight) {
        item.styleController.setHighlight(highlight)
      } else {
        item.styleController.setHighlight(false);
      }
    }
  }

  public selectedChipsFilterAggregate(text: string, matChipsList: OptionCategory[], type: "genre" | "year") {
    this.isParallelLoading = true;
    this.filteredList = [];
    let genreSelected: boolean | undefined;
    if (type === "year") {
      this.yearsChipList = matChipsList;

    } else {
      this.genreChipList = matChipsList;
      genreSelected = true;
    }
    for (const genre of this.genreChipList) {
      if (genre.isSelected) {
        this.filteredList.push(...this.itemDumpAggregate.filter(item => item.data.genres?.includes(genre.value)))
      }
    }

    for (const year of this.yearsChipList) {
      if (year.isSelected && !genreSelected) {
        this.filteredList = this.itemDumpAggregate?.filter(item => item.data.year.toString() === year.value);
      } else if (year.isSelected && genreSelected) {
        this.filteredList = this.filteredList?.filter(item => item.data.year.toString() === year.value);
      }
    }
    this.filterChipsCountSetUp();
    this.isParallelLoading = false;
  }

  subscribeToValueChanges() {
    this.isParallelLoading = true;
    this.valuChangeSubscription$ = this.control.valueChanges.subscribe(value => {
      if (!value || value.length === 0) {
        this.filteredList = [];
      } else {
        this.filteredList = this.itemDumpAggregate.filter(item => item.data?.title?.toLowerCase()?.startsWith(value.toLowerCase()));
      }
      this.isParallelLoading = false;
    });
  }

  selectedItemClicked(item: CinematicDumpAggregate) {
    this.isParallelLoading = true;
    forEach(this.itemDumpAggregate, (o) => {
      if(o.data.title === item.data.title) {
        o.styleController.setHighlight(false);
      }
    });
    this.selectedItemAggregate = item.data;
    this.routes?.navigate([`/${this.currentMode?.toLowerCase()}`], {
      relativeTo: this.activatedRoutes,
      skipLocationChange: false,
      queryParamsHandling: 'merge',
      replaceUrl: true,
      queryParams: {
        title: this.selectedItemAggregate.title ? this.selectedItemAggregate.title.toLowerCase().replace(' ', '') : ''
      }
    });
    this.itemStore.dispatch(setSelectedAggregate(this.selectedItemAggregate));
    if (this.context_type === "panel") {
      this.ref.selectedItemClick.emit();
    }
    this.isParallelLoading = false;
  }

  public filterChipsCountSetUp() {
    for (const chip of [...this.genreChipList, ...this.yearsChipList]) {
      if (this.filteredList.length) {
        chip.counts = this.filteredList.filter(item => item.data.year?.toString() === chip.value || item.data.genres?.includes(chip.value)).length;
      } else {
        chip.counts = this.itemAggregate.filter(item => item.year?.toString() === chip.value || item.genres?.includes(chip.value)).length;
      }
    }
  }

  public configPanelChips(): void {
    if (!this.ref.selectedPanelKey || !this.ref.selectedPanelValue) {
      return;
    }
    console.log(this.ref.selectedPanelKey);
    if (this.ref.selectedPanelKey === 'genres') {
      forEach(this.genreChipList, genre => {
        genre.isSelected = genre.key === this.ref.selectedPanelValue;
      })
    } else {
      forEach(this.yearsChipList, year => {
        year.isSelected = year.key === this.ref.selectedPanelValue;
      })
    }
  }

  private async subscribeToCurrentMode() {
    this.currentModeSubscription$ = this.itemStore.select(getCurrentMode).subscribe(currentMode => {
      this.currentMode = currentMode;
    })
  }
}
