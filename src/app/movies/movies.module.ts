import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoviesRoutingModule } from './movies-routing.module';
import { SharedModule } from '../shared/shared.module';
import { cinematicDumpReducer } from '../cinematic-dump-store/cd-reducers';
import { StoreModule } from '@ngrx/store';
import { MsMovieRootComponent } from './ms-movie-root/ms-movie-root.component';
import {ItemCardSvg} from "../shared/components/item-card/item-card.svg";
import {QuickItemCardSvg} from "../shared/components/quick-item-card/quick-item-card.svg";
import { ItemDetailedContentSvg } from '../shared/components/item-detailed-content/item-detailed-content.svg';
import {StructuralLoadingSvg} from "../shared/components/structural-loading/structural-loading.svg";
import {ItemsContainerComponent} from "../shared/components/items-container/items-container.component";
import {LoadingComponent} from "../shared/components/loading/loading.component";
import {
  ItemDetailedContentMobileSvg
} from "../shared/components/item-detailed-content-mobile/item-detailed-content-mobile.svg";

@NgModule({
  declarations: [
    MsMovieRootComponent
  ],
  imports: [
    CommonModule,
    MoviesRoutingModule,
    SharedModule,
    StoreModule.forFeature('cinematic-dumpState', cinematicDumpReducer),
    ItemCardSvg,
    QuickItemCardSvg,
    ItemDetailedContentSvg,
    StructuralLoadingSvg,
    ItemsContainerComponent,
    LoadingComponent,
    ItemDetailedContentMobileSvg
  ]
})
export class MoviesModule { }
