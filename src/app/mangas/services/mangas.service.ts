import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable, map, catchError} from 'rxjs';
import { environment } from 'src/environments/environment';
import {CinematicDumpAggregate, ItemAggregate} from "../../shared/models/cinematic-dump.model";

@Injectable({
  providedIn: 'root'
})
export class MangasService {

  // private _mangasAggregate: MangasAggregate[] = [];
  //
  // public set setMangasAggregate(movies: MangasAggregate[]) {
  //   this._mangasAggregate = movies;
  // }
  //
  // public get getMangasAggregate(): MangasAggregate[] {
  //   return this._mangasAggregate;
  // }

  constructor(private httpClient: HttpClient) { }

  getAggregate(): Observable<CinematicDumpAggregate[]> {
    return this.httpClient.get<ItemAggregate[]>(`${environment.webServiceUrl}/${environment.mangas_aggregate}`).pipe(
      map(items => {
        const cinematicDumpAggregate: CinematicDumpAggregate[] = [];
        items.forEach(item => {
          cinematicDumpAggregate.push(new CinematicDumpAggregate(item));
        });
        return cinematicDumpAggregate;
      }), catchError((err, caught) => {
        return [];
      })
    );
  }
}
