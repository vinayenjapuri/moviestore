import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MangaRootComponent} from "./components/manga-root/manga-root.component";

const routes: Routes = [
  { path: '', component: MangaRootComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MangasRoutingModule { }
