import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MangaRootComponent } from './manga-root.component';

describe('MangaRootComponent', () => {
  let component: MangaRootComponent;
  let fixture: ComponentFixture<MangaRootComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MangaRootComponent]
    });
    fixture = TestBed.createComponent(MangaRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
