import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractRootControl} from "../../../shared/abstract-classes/abstract-root-control";
import {Store} from "@ngrx/store";
import {CinematicDumpState} from "../../../cinematic-dump-store/cd-state";
import {ActivatedRoute} from "@angular/router";
import {MangasService} from "../../services/mangas.service";

@Component({
  selector: 'ms-manga-root',
  templateUrl: './manga-root.component.html',
  styleUrls: ['./manga-root.component.scss']
})
export class MangaRootComponent extends AbstractRootControl implements OnInit, OnDestroy {

  constructor(private mangasService: MangasService, private movieStore: Store<CinematicDumpState>, private activated: ActivatedRoute) {
    super(movieStore, mangasService, activated);
  }

  ngOnDestroy(): void {
    this.dataSubscription?.unsubscribe();
    this.cinematicDumpAggregateSubscription?.unsubscribe();
    this.queryParamsSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.registerConfigurationImplementation().then(() => {

    });
  }

}
