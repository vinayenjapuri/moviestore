import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MangasRoutingModule } from './mangas-routing.module';
import { SharedModule } from '../shared/shared.module';
import { cinematicDumpReducer } from '../cinematic-dump-store/cd-reducers';
import { StoreModule } from '@ngrx/store';
import { MangaRootComponent } from './components/manga-root/manga-root.component';
import {ItemDetailedContentSvg} from "../shared/components/item-detailed-content/item-detailed-content.svg";
import {ItemsContainerComponent} from "../shared/components/items-container/items-container.component";
import {LoadingComponent} from "../shared/components/loading/loading.component";



@NgModule({
  declarations: [
    MangaRootComponent
  ],
  imports: [
    CommonModule,
    MangasRoutingModule,
    SharedModule,
    StoreModule.forFeature('cinematic-dumpState', cinematicDumpReducer),
    ItemDetailedContentSvg,
    ItemsContainerComponent,
    LoadingComponent
  ]
})
export class MangasModule { }
