import {CinematicDumpAggregate, ItemAggregate} from "../models/cinematic-dump.model";
import {Subscription} from "rxjs";
import {SeriesService} from "../../series/series.service";
import {Store} from "@ngrx/store";
import {CinematicDumpState} from "../../cinematic-dump-store/cd-state";
import {
  setAnimesAggregate,
  setMangasAggregate,
  setMode,
  setMoviesAggregate,
  setSelectedAggregate,
  setSeriesAggregate
} from '../../cinematic-dump-store/cd-action';
import {forEach} from "lodash";
import {AbstractItemStyle} from "../components/abstract-item-style";
import {MangasService} from "../../mangas/services/mangas.service";
import {AnimesService} from "../../animes/services/animes.service";
import {MoviesService} from '../../movies/services/movies.service';
import {
  getAnimesAggregate,
  getCurrentMode,
  getMangasAggregate,
  getMoviesAggregate,
  getSelectedAggregate,
  getSeriesAggregate
} from '../../cinematic-dump-store/cd-selectors';
import {bodyConfigSetUp} from "./app-control";
import {CINEMATIC_DUMP_MODE_TYPES} from "../enums/cinematic-dump-mode-types";
import {ActivatedRoute} from "@angular/router";

export type Service = SeriesService | MoviesService | AnimesService | MangasService;

export class AbstractRootControl {
  public itemAggregates?: ItemAggregate[];

  public dataSubscription?: Subscription;

  public cinematicDumpAggregateSubscription?: Subscription;

  public queryParamsSubscription?: Subscription;

  public mobileResolutionSubscription?: Subscription;

  public aggregate: CinematicDumpAggregate[] = [];

  public selectedCinematicDumpAggregate?: ItemAggregate;

  private queryTitle?: string;

  public isLoading: boolean = false;

  public isMobileResolution: boolean = false;

  public queryGenre?: string;

  public currentModeSubscription$?: Subscription;

  public currentMode?: CINEMATIC_DUMP_MODE_TYPES;

  constructor(private store: Store<CinematicDumpState>,
              private service: Service, private activatedRoute: ActivatedRoute) {
  }
  public async registerConfigurationImplementation(): Promise<void> {
    await this.subscribeToItemAggregates();
    this.getItemAggregates();
    this.subscribeToSelectedCinematicDumpAggregate();
    this.setupSelectedAggregateDataOnRoute();
    this.subscribeToCurrentMode();
    this.subscribeToSelectedItemAggregateOnRoute();
  }

  getItemAggregates() {
    if (!this.itemAggregates?.length) {
      this.isLoading = true;
      this.cinematicDumpAggregateSubscription = this.service.getAggregate()?.subscribe(aggregate => {
        this.aggregate = aggregate;
        this.itemAggregates = aggregate.map(o => o.data) ?? [];
        this.store.dispatch(this.getStoreAction()(this.itemAggregates));
        this.setupSelectedAggregateDataOnRoute();
        this.isLoading = false;
      })
    }
  }

  async subscribeToItemAggregates() {
    this.isLoading = true;
    this.dataSubscription = this.store.select(this.getStoreSelector()).subscribe(items => {
      if (!this.itemAggregates?.length) {
        this.itemAggregates = items ?? [];
        if (this.itemAggregates?.length) {
          forEach(this.itemAggregates, (item) => {
            this.aggregate.push({data: item, styleController: new AbstractItemStyle()});
          });
          this.setupSelectedAggregateDataOnRoute();
          this.isLoading = false;
        }
      }

    });
  }

   protected getStoreSelector() {
    switch (this.currentMode) {
      case CINEMATIC_DUMP_MODE_TYPES.MOVIES:
        return getMoviesAggregate;
      case CINEMATIC_DUMP_MODE_TYPES.ANIMES:
        return getAnimesAggregate;
      case CINEMATIC_DUMP_MODE_TYPES.MANGAS:
        return getMangasAggregate;
      default:
        return getSeriesAggregate;
    }
  }

  protected getStoreAction() {
    switch (this.currentMode) {
      case CINEMATIC_DUMP_MODE_TYPES.MOVIES:
        return setMoviesAggregate;
      case CINEMATIC_DUMP_MODE_TYPES.ANIMES:
        return setAnimesAggregate;
      case CINEMATIC_DUMP_MODE_TYPES.MANGAS:
        return setMangasAggregate;
      default:
        return setSeriesAggregate;
    }
  }

  private subscribeToSelectedCinematicDumpAggregate(): void {
    this.cinematicDumpAggregateSubscription = this.store.select(getSelectedAggregate).subscribe(aggregate => {
      this.selectedCinematicDumpAggregate = aggregate;
    });
  }

  private subscribeToSelectedItemAggregateOnRoute() {
    this.store.dispatch(setMode(bodyConfigSetUp()));
    this.queryParamsSubscription = this.activatedRoute?.queryParams.subscribe(query => {
      this.queryTitle = query['title'];
      this.queryGenre = query['genre'];
    });

  }

  private setupSelectedAggregateDataOnRoute() {
    if (this.queryTitle) {
      this.isLoading = true;
      forEach(this.aggregate, (item) => {
        if (this.queryTitle === item.data.title?.toLowerCase().replace(' ', '')) {
          this.store.dispatch(setSelectedAggregate(item.data));
        }
      });
      this.isLoading = false;
    }
  }

  private subscribeToCurrentMode() {
    this.currentModeSubscription$ = this.store.select(getCurrentMode).subscribe(currentMode => {
      this.currentMode = currentMode;
    })
  }
}
