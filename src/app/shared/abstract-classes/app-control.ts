import {CINEMATIC_DUMP_MODE_TYPES} from "../enums/cinematic-dump-mode-types";


export function bodyConfigSetUp(): CINEMATIC_DUMP_MODE_TYPES {
  const url = window.document.URL;
  if (url.indexOf('movies')>-1) {
    return CINEMATIC_DUMP_MODE_TYPES.MOVIES;
  } else if (url.indexOf('animes')>-1) {
    return CINEMATIC_DUMP_MODE_TYPES.ANIMES
  } else if (url.indexOf('mangas')>-1) {
    return CINEMATIC_DUMP_MODE_TYPES.MANGAS
  } else if (url.indexOf('series')> -1) {
    return CINEMATIC_DUMP_MODE_TYPES.SERIES

  } else {
    return CINEMATIC_DUMP_MODE_TYPES.NONE
  }
}
