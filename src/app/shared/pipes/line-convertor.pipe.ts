import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lineConvertor',
  standalone: true
})
export class LineConvertorPipe implements PipeTransform {

  transform(value?: string, to: number = 28, from: number = 0, ellipsis?: boolean, type: "desc"|"title" = "title"): string {
    if (!value) {
      return '';
    }
    if (type==="title") {
      if (ellipsis && value.length > to) {
        return value.substring(from, to) + '..';
      }
      if (ellipsis && value.length <=to) {
        return value;
      }
      if (to === 28) {
        return value.substring(from, value.length>28? value.substring(0, 28).lastIndexOf(' '): value.length);
      } else {
        return value.substring(value.lastIndexOf(' ')+1, value.length || to);
      }
    } else {
      if (ellipsis) {
        let text = value.substring(from, value.length);
        return text.substring(text.indexOf(' ')+1, value.length>to ? value.substring(0, to).lastIndexOf(' '): value.length) + '..';
      } else {
        return value.substring(from, value.length<to? value.length : value.substring(0,to).lastIndexOf(' '))
      }

    }

  }

}
