import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contentSetter',
  standalone: true
})
export class SetXWidthPipe implements PipeTransform {

  //For font-size 36,
  transform(value: number, type: 'width' | 'font'): number {
    if (value>407 && value < 509) {
      return 28;
    } else if (value<407 && value>240) {
      return 22;
    } else {
      return 34;
    }
  }

}
