import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'textWrapper',
  standalone: true
})
export class TextWrapperPipe implements PipeTransform {

  transform(text?: string, from?: number, to?: number, ellipsis?: boolean, isDescription?: boolean, firstLine?: boolean, secondLine?: boolean): string {
    let subText: string = '';
    if (!text || !to) {
     return '';
    }
    if (isDescription) {
      if(text.length > 100 && firstLine) {
        subText+= text.substring(from!, to).concat(...['-'])
      } else if (text.length < to) {
        return text;
      } else {
        subText+= text.substring(from!, to!)+'..';
      }
    } else {
      subText+=(text.length >=to && ellipsis ? text.substring(from!, to) + '...': text);
    }


    return subText;
  }

}
