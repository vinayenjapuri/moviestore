export class SvgSanitizer {
  name: string;
  path: string;
  svg_name: string;

  constructor(svg: SvgSanitizer) {
    this.name = svg.name;
    this.path = svg.path;
    this.svg_name = svg.svg_name;

  }
}
