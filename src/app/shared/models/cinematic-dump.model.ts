import {AbstractItemStyle} from "../components/abstract-item-style";

export class CinematicDumpAggregate {
  data: ItemAggregate;
  styleController: AbstractItemStyle;

  constructor(item: ItemAggregate, styleController?: AbstractItemStyle) {
    this.data = item;
    this.styleController = styleController ?? new AbstractItemStyle();
  }

}

export class ItemAggregate {
  title: string;
  chapters: number;
  episodes: number;
  genres: string[];
  highlighted: boolean;
  director: Director;
  characters: Actors[];
  imdb_rating: number;
  accredited: string;
  year: number;
  description: string;
  seasons: number;
  trailer: string;
  shown: ShownAggregate[];
  reviews: ReviewsAggregate[];
  viewed: number;
  author: Director;
  voice_actors: string[];
  actors: Actors[];
  overviewImage: string;
  _id: any;
  pg: number;
  cbfc: string;
  creators: string[];

  constructor(item: ItemAggregate) {
    this._id = item._id;
    this.title = item.title ;
    this.chapters = item.chapters ;
    this.episodes = item.episodes ;
    this.genres = item.genres;
    this.highlighted = item.highlighted ?? false;
    this.director = item.director;
    this.characters = item.characters ;
    this.imdb_rating = item.imdb_rating ;
    this.accredited = item.accredited ;
    this.year = item.year ;
    this.description = item.description ;
    this.seasons = item.seasons ;
    this.shown = item.shown ;
    this.reviews = item.reviews ;
    this.viewed = item.viewed ;
    this.author = item.author ;
    this.trailer = item.trailer ;
    this.voice_actors = item.voice_actors ;
    this.actors = item.actors ;
    this.overviewImage = item.overviewImage ;
    this.pg = item.pg ;
    this.cbfc = item.cbfc ;
    this.creators = item.creators ;
  }

}



export interface ReviewsAggregate {
  reviewer?: string;
  review_message?: string;
  rating?: number;
  total_ratings?: number;
}

export interface Director {
  name?: string;
  about?: string;
  age?: number;
  gender?: string;
  worked_in?: string[];
}

export interface Actors {
  name?: string;
  about?: string;
  gender?: string;
  age?: number;
  worked_in?: string[];
  active?: boolean;

}

export interface ShownAggregate {
  name?: string;
  url?: string;
}

