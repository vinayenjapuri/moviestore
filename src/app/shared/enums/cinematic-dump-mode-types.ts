export enum CINEMATIC_DUMP_MODE_TYPES {
  NONE = "None",
  ANIMES = "Animes",
  MANGAS = "Mangas",
  SERIES = "Series",
  MOVIES = "Movies"
 }
