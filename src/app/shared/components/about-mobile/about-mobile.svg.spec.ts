import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutMobileSvg } from './about-mobile.svg';

describe('AboutMobileSvg', () => {
  let component: AboutMobileSvg;
  let fixture: ComponentFixture<AboutMobileSvg>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AboutMobileSvg]
    });
    fixture = TestBed.createComponent(AboutMobileSvg);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
