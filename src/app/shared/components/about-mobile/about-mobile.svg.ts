import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'ms-about-mobile',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './about-mobile.svg.html',
  styleUrls: ['./about-mobile.svg.scss']
})
export class AboutMobileSvg {

}
