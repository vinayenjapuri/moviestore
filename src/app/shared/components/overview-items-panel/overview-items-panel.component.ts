import {Component, Inject, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from "@angular/material/bottom-sheet";
import {Store} from "@ngrx/store";
import {CinematicDumpState} from "../../../cinematic-dump-store/cd-state";
import {CINEMATIC_DUMP_MODE_TYPES} from "../../enums/cinematic-dump-mode-types";
import {
  getAnimesAggregate,
  getMangasAggregate,
  getMoviesAggregate,
  getSeriesAggregate
} from "../../../cinematic-dump-store/cd-selectors";
import {CinematicDumpAggregate} from "../../models/cinematic-dump.model";
import {forEach} from "lodash";
import {AbstractItemStyle} from "../abstract-item-style";
import {SharedModule} from "../../shared.module";
import {ItemsContainerComponent} from "../items-container/items-container.component";
import {LoadingComponent} from "../loading/loading.component";
import {ItemCardMobileSvg} from "../item-card-mobile/item-card-mobile.svg";
import {ItemCardSvg} from "../item-card/item-card.svg";

@Component({
  selector: 'ms-overview-items-panel',
  standalone: true,
  imports: [CommonModule, SharedModule, ItemsContainerComponent, LoadingComponent, ItemCardMobileSvg, ItemCardSvg],
  templateUrl: './overview-items-panel.component.html',
  styleUrls: ['./overview-items-panel.component.scss']
})
export class OverviewItemsPanelComponent implements OnInit {

  public cinematicAggregate: CinematicDumpAggregate[] = [];

  public isLoading: boolean = false;

  constructor(private matBottomSheetRef: MatBottomSheetRef,
              @Inject(MAT_BOTTOM_SHEET_DATA) public data: {type: CINEMATIC_DUMP_MODE_TYPES, context: "genres" | "year", value: string}, private store: Store<CinematicDumpState>) {

  }
  ngOnInit(): void {
    this.configItemsAggregate();
  }

  closePanel() {
    this.matBottomSheetRef.dismiss();
  }

  private configItemsAggregate(): void {
    this.isLoading = true;
    this.store.select(this.getSelectorsAggregate()).subscribe(aggregate => {
      if (aggregate){
        forEach(aggregate, item => {
          if (item.genres?.includes(this.data.value) ?? item.year?.toString() === this.data.value) {
            const aggrItem: CinematicDumpAggregate = {data: item, styleController: new AbstractItemStyle()};
            this.cinematicAggregate.push(aggrItem);
          }
        });
      }
      this.isLoading = false;
    });
  }

  private getSelectorsAggregate() {
    switch (this.data.type) {
      case CINEMATIC_DUMP_MODE_TYPES.MANGAS:
        return getMangasAggregate;
      case CINEMATIC_DUMP_MODE_TYPES.ANIMES:
        return getAnimesAggregate;
      case CINEMATIC_DUMP_MODE_TYPES.MOVIES:
        return getMoviesAggregate;
      default:
        return getSeriesAggregate;
    }
  }
}
