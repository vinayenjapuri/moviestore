import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OverviewItemsPanelComponent } from './overview-items-panel.component';

describe('OverviewItemsPanelComponent', () => {
  let component: OverviewItemsPanelComponent;
  let fixture: ComponentFixture<OverviewItemsPanelComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [OverviewItemsPanelComponent]
    });
    fixture = TestBed.createComponent(OverviewItemsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
