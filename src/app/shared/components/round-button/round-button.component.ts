import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'ms-round-button',
  templateUrl: './round-button.component.html',
  styleUrls: ['./round-button.component.scss']
})
export class RoundButtonComponent {

  @Input({required: true})
  name: string = '';

  @Input()
  highlight: boolean = false;

  @Output()
  onClick: EventEmitter<string> = new EventEmitter<string>();

  buttonClick() {
    this.onClick.emit(this.name);
  }

}
