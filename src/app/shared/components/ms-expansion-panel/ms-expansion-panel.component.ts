import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'ms-expansion-panel',
  templateUrl: './ms-expansion-panel.component.html',
  styleUrls: ['./ms-expansion-panel.component.scss']
})
export class MsExpansionPanelComponent implements OnInit {

  @Input()
  yearsChipList: any[] = [];

  @Input()
  genreChipList: any[] = [];

  @Input()
  contextType?: "panel" | "document";

  @Input()
  chipType?: "genres" | "year";

  @Output()
  onClick: EventEmitter<any> = new EventEmitter<any>();

  public hideExpansionPanel: boolean = false;

  ngOnInit(): void {
    if (!this.genreChipList.length && !this.yearsChipList.length) {
      this.hideExpansionPanel = true;
    }
  }

  chipSelected($event: any, chip: string) {
    this.onClick.emit([...$event, chip]);
  }

}
