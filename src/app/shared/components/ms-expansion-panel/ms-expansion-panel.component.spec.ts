import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MsExpansionPanelComponent } from './ms-expansion-panel.component';

describe('MsExpansionPanelComponent', () => {
  let component: MsExpansionPanelComponent;
  let fixture: ComponentFixture<MsExpansionPanelComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MsExpansionPanelComponent]
    });
    fixture = TestBed.createComponent(MsExpansionPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
