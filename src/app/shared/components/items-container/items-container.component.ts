import {ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {AbstractItemControl} from "../../../movies/abstract-item-control";
import {Store} from "@ngrx/store";
import {CinematicDumpState} from "../../../cinematic-dump-store/cd-state";
import {MatSnackBar} from "@angular/material/snack-bar";
import {CinematicDumpAggregate} from "../../models/cinematic-dump.model";
import {ActivatedRoute, Router} from "@angular/router";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../../shared.module";
import {ItemCardSvg} from "../item-card/item-card.svg";
import {QuickItemCardSvg} from "../quick-item-card/quick-item-card.svg";
import {AdsenseModule} from "ng2-adsense";
import {getMobileResolution} from "../../../cinematic-dump-store/cd-selectors";
import {Subscription} from "rxjs";
import {ItemCardMobileSvg} from "../item-card-mobile/item-card-mobile.svg";
import {MatExpansionModule} from "@angular/material/expansion";
import {EmptyStateComponent} from "../empty-state/empty-state.component";
import {LoadingComponent} from "../loading/loading.component";

@Component({
  selector: 'ms-items-container',
  templateUrl: './items-container.component.html',
  standalone: true,
  imports: [CommonModule, SharedModule, ItemCardSvg, QuickItemCardSvg, AdsenseModule, ItemCardMobileSvg, MatExpansionModule, EmptyStateComponent, LoadingComponent]
})
export class ItemsContainerComponent extends AbstractItemControl implements OnInit, OnDestroy {

  private _aggregate?: CinematicDumpAggregate[];
  public isMobileResolution: boolean = false;
  private windowResizeSubscription?: Subscription;

  @Input({required: true})
  set aggregate(dumpAggregates: CinematicDumpAggregate[]) {
    this._aggregate = dumpAggregates ?? [];
    this.itemDumpAggregate = this.aggregate;
  }

  @Input()
  selectedGenre?: string;

  @Input({required: true})
  contextType?: "panel" | "document";

  @Input()
  public selectedPanelKey?: "genres" | "year";

  @Input()
  public selectedPanelValue?: string;

  @Output()
  selectedItemClick: EventEmitter<void> = new EventEmitter<void>();

  get aggregate(): CinematicDumpAggregate[] {
    return this._aggregate ?? [];
  }

  constructor(private store: Store<CinematicDumpState>, private changeDetectorRef: ChangeDetectorRef, private snackBarRef: MatSnackBar, private routers: Router, private activatedRoute: ActivatedRoute) {
    super(store, snackBarRef, routers, activatedRoute);
    this.ref = this;
  }

  ngOnInit(): void {
    this.subscribeToValueChanges();
    this.subscribeToWindowResize();
    this.context_type = this.contextType;

    this.configPanelChips();
  }

  public onValueChanges(event: string): void {
    console.log(event);
  }

  ngOnDestroy() {
    this.itemSubscription?.unsubscribe();
    this.windowResizeSubscription?.unsubscribe();
    this.currentModeSubscription$?.unsubscribe();
    this.valuChangeSubscription$?.unsubscribe();
  }

  subscribeToWindowResize(): void {
    this.windowResizeSubscription = this.store.select(getMobileResolution).subscribe(isMobileResolution => {
      this.isMobileResolution = isMobileResolution!;
    });
  }
}
