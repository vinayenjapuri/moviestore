import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ItemAggregate} from "../../models/cinematic-dump.model";
import {Store} from "@ngrx/store";
import {CinematicDumpState} from "../../../cinematic-dump-store/cd-state";
import {getCurrentMode, getInnerWidth} from "../../../cinematic-dump-store/cd-selectors";
import {Subscription} from "rxjs";
import {SetXWidthPipe} from "../../pipes/set-xwidth.pipe";
import {FlexModule} from "@angular/flex-layout";
import {SharedModule} from "../../shared.module";
import {OverviewItemsPanelComponent} from "../overview-items-panel/overview-items-panel.component";
import {MatBottomSheet} from "@angular/material/bottom-sheet";
import {CINEMATIC_DUMP_MODE_TYPES} from "../../enums/cinematic-dump-mode-types";
import {MatTooltipModule} from "@angular/material/tooltip";
import {setSelectedAggregate} from "../../../cinematic-dump-store/cd-action";
import {Router} from "@angular/router";

@Component({
  selector: 'ms-item-detailed-content-mobile',
  standalone: true,
  imports: [CommonModule, SetXWidthPipe, FlexModule, SharedModule, MatTooltipModule],
  templateUrl: './item-detailed-content-mobile.svg.html',
  styleUrls: ['./item-detailed-content-mobile.svg.scss']
})
export class ItemDetailedContentMobileSvg implements OnInit, OnDestroy {

  private _item?: ItemAggregate;

  public genresXList: {x: number, key: string, y?: number}[] = [];


  @Input({required: true})
  set item(item: ItemAggregate | undefined) {
    this._item = item;
    if (item?.genres) {
      item.genres?.forEach((genre, i) => {
        switch (i) {
          case 0:
            this.genresXList.push({x: 20, key: genre, y: 505});
          break;
          case 5:
            this.genresXList.push({x: 20, key: genre, y: 525});
          break;
          default:
            let x = this.genresXList[i-1].x;
            this.genresXList.push({x: this.genresXList[i-1].key.length>7 ? x+80:x+60, key: genre, y: i>5 ? 525: 505});
          break;
        }
      });
    }
  }

  get item(): ItemAggregate | undefined {
    return this._item;
  }

  width: number = 920;
  private innerWidthSubscription?: Subscription;
  private currentModeSubscription$?: Subscription;

  private currentMode?: CINEMATIC_DUMP_MODE_TYPES;

  constructor(private store: Store<CinematicDumpState>, private router: Router, private bottomSheet: MatBottomSheet) {
    this.width =window.innerWidth;
  }

  ngOnInit(): void {
    this.subscribeToCurrentMode();
    this.subscribeToInnerWidth();
  }

  private subscribeToInnerWidth(): void {
    this.innerWidthSubscription = this.store.select(getInnerWidth).subscribe(width => {
      this.width = width!;
    });
  }

  ngOnDestroy(): void {
    this.innerWidthSubscription?.unsubscribe();
    this.currentModeSubscription$?.unsubscribe();
  }

  onClickGenres(genre: string) {
    this.bottomSheet.open(OverviewItemsPanelComponent, {
      disableClose: false,
      hasBackdrop: true,
      direction: "ltr",
      data: {
        type: this.currentMode,
        context: "genres",
        value: genre
      }
    });
  }

  private subscribeToCurrentMode(): void {
    this.currentModeSubscription$ = this.store.select(getCurrentMode).subscribe(currentMode => {
      this.currentMode = currentMode;
    })
  }

  onClickYear(year: number | undefined): void {
    this.bottomSheet.open(OverviewItemsPanelComponent, {
      disableClose: false,
      hasBackdrop: true,
      direction: "ltr",
      data: {
        type: this.currentMode,
        context: "year",
        value: year
      }
    });
  }

  onClickExit() {
    this.router.navigate([`${this.currentMode?.toString().toLowerCase()}`], {
      queryParams: undefined,
      skipLocationChange: false
    }).then(() => {
      this.store.dispatch(setSelectedAggregate(undefined));
    });

  }

}
