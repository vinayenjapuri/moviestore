import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemDetailedContentMobileSvg } from './item-detailed-content-mobile.svg';

describe('ItemDetailedContentMobileSvg', () => {
  let component: ItemDetailedContentMobileSvg;
  let fixture: ComponentFixture<ItemDetailedContentMobileSvg>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ItemDetailedContentMobileSvg]
    });
    fixture = TestBed.createComponent(ItemDetailedContentMobileSvg);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
