import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutSvg } from './about.svg';

describe('AboutSvg', () => {
  let component: AboutSvg;
  let fixture: ComponentFixture<AboutSvg>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AboutSvg]
    });
    fixture = TestBed.createComponent(AboutSvg);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
