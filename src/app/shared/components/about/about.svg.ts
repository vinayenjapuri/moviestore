import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'ms-about',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './about.svg.html',
  styleUrls: ['./about.svg.scss']
})
export class AboutSvg {

}
