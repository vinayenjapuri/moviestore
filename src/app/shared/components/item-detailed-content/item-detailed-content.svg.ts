import {Component, OnInit, OnDestroy, Input} from '@angular/core';
import { CinematicDumpState } from '../../../cinematic-dump-store/cd-state';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { setSelectedAggregate } from '../../../cinematic-dump-store/cd-action';
import { Subscription } from 'rxjs';
import {getCurrentMode, getInnerWidth, getSelectedAggregate} from '../../../cinematic-dump-store/cd-selectors';
import { ItemAggregate } from '../../models/cinematic-dump.model';
import {CommonModule} from "@angular/common";
import {SharedModule} from "../../shared.module";
import {LineConvertorPipe} from "../../pipes/line-convertor.pipe";
import {MatBottomSheet} from "@angular/material/bottom-sheet";
import {OverviewItemsPanelComponent} from "../overview-items-panel/overview-items-panel.component";
import {CINEMATIC_DUMP_MODE_TYPES} from "../../enums/cinematic-dump-mode-types";

@Component({
  selector: 'ms-item-detailed-content',
  templateUrl: './item-detailed-content.svg.html',
  styleUrls: ['./item-detailed-content.svg.scss'],
  standalone: true,
  imports: [CommonModule, SharedModule, LineConvertorPipe]
})
export class ItemDetailedContentSvg implements OnInit, OnDestroy {

  private selectedUserSubscription?: Subscription;

  private _item?: ItemAggregate;

  private currentMode?: CINEMATIC_DUMP_MODE_TYPES;

  public width: number = window.innerWidth;

  public genresXList: {x: number, key: string}[] = [];
  private windowResizeSubscription?: Subscription;
  private currentModeSubscription$?: Subscription;


  @Input({required: true})
  set item(item: ItemAggregate | undefined) {
    this._item = item;
    if (item) {
      item.genres?.forEach((genre, i) => {
        if (i===0) {
          this.genresXList.push({x: 341.45, key: genre});
        } else {
          const x = this.genresXList[i-1].x;
          this.genresXList.push({x: this.genresXList[i-1].key.length>7 ? x+100:x+80, key: genre})
        }
      });
    }
  }

  get item(): ItemAggregate | undefined {
    return this._item;
  }


  constructor(private router: Router, private store: Store<CinematicDumpState>, private bottomSheet: MatBottomSheet) {
  }


  ngOnDestroy(): void {
    this.selectedUserSubscription?.unsubscribe();
    this.windowResizeSubscription?.unsubscribe();
    this.currentModeSubscription$?.unsubscribe();
  }

  ngOnInit(): void {
    this.subscribeToCurrentModes();
    this.subscribeToSelectedCDAggregate();
    this.subscribeToWidthChanges();
  }

  subscribeToSelectedCDAggregate() {
    this.selectedUserSubscription = this.store.select(getSelectedAggregate).subscribe(aggregate => {
      this.item = aggregate;
    });
  }

  onClickExit() {
    this.router.navigate([`${this.currentMode?.toString().toLowerCase()}`], {
      queryParams: undefined,
      skipLocationChange: false
    }).then(() => {
      this.store.dispatch(setSelectedAggregate(undefined));
    });

  }

  onClickGenre($event: string) {
    this.bottomSheet.open(OverviewItemsPanelComponent, {
      disableClose: false,
      hasBackdrop: true,
      direction: "ltr",
      data: {
        type: this.currentMode,
        context: "genres",
        value: $event
      }
    });
  }

  private subscribeToCurrentModes() {
    this.currentModeSubscription$ = this.store.select(getCurrentMode).subscribe(currentMode => {
      this.currentMode = currentMode;
    })
  }
  private subscribeToWidthChanges(): void {
    this.windowResizeSubscription = this.store.select(getInnerWidth).subscribe(width => {
      this.width = width!;
    })
  }
}
