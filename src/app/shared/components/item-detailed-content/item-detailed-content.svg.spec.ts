import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemDetailedContentSvg } from './item-detailed-content.svg';

describe('ItemDetailedContentSvg', () => {
  let component: ItemDetailedContentSvg;
  let fixture: ComponentFixture<ItemDetailedContentSvg>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ItemDetailedContentSvg]
    });
    fixture = TestBed.createComponent(ItemDetailedContentSvg);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
