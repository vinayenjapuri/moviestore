import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemCardMobileSvg } from './item-card-mobile.svg';

describe('ItemCardMobileSvg', () => {
  let component: ItemCardMobileSvg;
  let fixture: ComponentFixture<ItemCardMobileSvg>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ItemCardMobileSvg]
    });
    fixture = TestBed.createComponent(ItemCardMobileSvg);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
