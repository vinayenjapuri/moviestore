import {Component, Input} from '@angular/core';
import { CommonModule } from '@angular/common';
import {ItemAggregate} from "../../models/cinematic-dump.model";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatMenuModule} from "@angular/material/menu";

@Component({
  selector: 'ms-item-card-mobile',
  standalone: true,
  imports: [CommonModule, MatTooltipModule, MatMenuModule],
  templateUrl: './item-card-mobile.svg.html',
  styleUrls: ['./item-card-mobile.svg.scss']
})
export class ItemCardMobileSvg {

  @Input({required: true})
  item!: ItemAggregate;

  @Input()
  highlighted: boolean = false;

}
