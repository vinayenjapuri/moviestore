import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemCardSvg } from './item-card.svg';

describe('ItemCardSvg', () => {
  let component: ItemCardSvg;
  let fixture: ComponentFixture<ItemCardSvg>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ItemCardSvg]
    });
    fixture = TestBed.createComponent(ItemCardSvg);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
