import { Component, Input } from '@angular/core';
import {TextWrapperPipe} from "../../pipes/text-wrapper.pipe";
import {ItemAggregate} from "../../models/cinematic-dump.model";
import {CommonModule} from "@angular/common";
import {EclipseComponent} from "../eclipse/eclipse.component";
import {LineConvertorPipe} from "../../pipes/line-convertor.pipe";

export type Item = ItemAggregate;

@Component({
  selector: 'ms-item-card',
  templateUrl: './item-card.svg.html',
  styleUrls: ['./item-card.svg.scss'],
  standalone: true,
  imports: [CommonModule, TextWrapperPipe, EclipseComponent, LineConvertorPipe]
})
export class ItemCardSvg {

  private _item!: Item;

  public backgroundImageStyle = {};

  @Input({required: true})
  set item(item: Item) {
    this._item = item;
    this.backgroundImageStyle = {
      "xlink:href": item.overviewImage,
      "background-repeat": "no-repeat",
      "background-size": "100%",
      "x":7,
      "width": "560px",
      "height": "131px"


    }
  }

  @Input({required: true})
  highlighted: boolean = false;

  get item(): Item {
    return this._item;
  }


}
