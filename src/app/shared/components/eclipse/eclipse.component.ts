import {Component, Input} from '@angular/core';
import {CommonModule, NgOptimizedImage} from "@angular/common";

@Component({
  selector: 'ms-eclipse',
  templateUrl: './eclipse.component.html',
  styleUrls: ['./eclipse.component.scss'],
  standalone: true,
  imports: [CommonModule, NgOptimizedImage]
})
export class EclipseComponent {

  @Input()
  link: string = '';

}
