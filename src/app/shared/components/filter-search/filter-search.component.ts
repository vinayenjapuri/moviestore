import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Subscription} from "rxjs";

@Component({
  selector: 'ms-filter-search',
  templateUrl: './filter-search.component.html',
  styleUrls: ['./filter-search.component.scss']
})
export class FilterSearchComponent implements OnInit, OnDestroy {

  @Input({required: true})
  control!: FormControl;

  @Input({required: true}) placeholder!: string;

  @Input({required: true}) label!: string;

  @Output()
  public valueChanged: EventEmitter<string> = new EventEmitter<string>();
  private inputSubscription$?: Subscription;

  ngOnInit() {
    this.inputSubscription$ = this.control.valueChanges.subscribe(value => {
      this.valueChanged.emit(value);
    });
  }

  ngOnDestroy(): void {
    this.inputSubscription$?.unsubscribe();
  }

}
