import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MatChipListboxChange} from "@angular/material/chips";
import {forEach} from "lodash";

@Component({
  selector: 'ms-selection-chips',
  templateUrl: './selection-chips.component.html',
  styleUrls: ['./selection-chips.component.scss']
})
export class SelectionChipsComponent {

  @Input({required: true})
  matChipList: { key: string, value: string, isSelected: boolean, counts: number }[] = [];

  @Output()
  public selectedEvent: EventEmitter<[string, { key: string, value: string, isSelected: boolean }[]]> = new EventEmitter<[string, {key: string; value: string; isSelected: boolean}[]]>();

  @Input()
  contextType?: "panel" | "document";

  @Input()
  chipType?: "genres" | "year";

  @Input()
  chipInputs?: "genre" | "year";
  public chipSelected($event: MatChipListboxChange) {
    forEach(this.matChipList, (chip) => {
      chip.isSelected = false;
      (($event.source.selected && ($event.value as string).includes(chip.value)) && (chip.isSelected = true))
    });
    this.selectedEvent.emit([$event.value, this.matChipList]);
  }

}
