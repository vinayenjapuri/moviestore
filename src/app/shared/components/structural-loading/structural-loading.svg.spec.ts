import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StructuralLoadingSvg } from './structural-loading.svg';

describe('StructuralLoadingSvg', () => {
  let component: StructuralLoadingSvg;
  let fixture: ComponentFixture<StructuralLoadingSvg>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StructuralLoadingSvg]
    });
    fixture = TestBed.createComponent(StructuralLoadingSvg);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
