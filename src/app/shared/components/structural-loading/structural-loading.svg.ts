import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'ms-structural-loading',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './structural-loading.svg.html',
  styleUrls: ['./structural-loading.svg.scss']
})
export class StructuralLoadingSvg {

}
