
export class AbstractItemStyle {
  public highlighted: boolean = false;
  public showTooltip: boolean = false;

  setHighlight(highlight: boolean) {
    this.highlighted = highlight;
  }

  tooltipToggle(showTooltip: boolean): void {
    this.showTooltip = showTooltip;
  }

}
