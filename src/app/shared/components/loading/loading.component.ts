import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FlexModule} from "@angular/flex-layout";

@Component({
  selector: 'ms-loading',
  standalone: true,
    imports: [CommonModule, FlexModule],
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent {

}
