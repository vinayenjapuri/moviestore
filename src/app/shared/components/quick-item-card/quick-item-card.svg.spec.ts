import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickItemCardSvg } from './quick-item-card.svg';

describe('QuickItemCardSvg', () => {
  let component: QuickItemCardSvg;
  let fixture: ComponentFixture<QuickItemCardSvg>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [QuickItemCardSvg]
    });
    fixture = TestBed.createComponent(QuickItemCardSvg);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
