import { Component } from '@angular/core';
import {CommonModule} from "@angular/common";

@Component({
  selector: 'ms-quick-item-card',
  templateUrl: './quick-item-card.svg.html',
  styleUrls: ['./quick-item-card.svg.scss'],
  standalone: true,
  imports: [CommonModule]
})
export class QuickItemCardSvg {
  public visible: boolean = false;

  showOverlay() {
    this.visible = true;
  }

  hideOverlay() {
    this.visible = false;
  }

}
