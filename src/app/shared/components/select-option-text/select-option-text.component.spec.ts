import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectOptionTextComponent } from './select-option-text.component';

describe('SelectOptionTextComponent', () => {
  let component: SelectOptionTextComponent;
  let fixture: ComponentFixture<SelectOptionTextComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SelectOptionTextComponent]
    });
    fixture = TestBed.createComponent(SelectOptionTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
