import { NgModule } from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {MatDividerModule} from '@angular/material/divider';
import {MatCardModule} from '@angular/material/card';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { MatSliderModule} from '@angular/material/slider';
import { MatProgressBarModule} from '@angular/material/progress-bar';
import { MatDialogModule} from '@angular/material/dialog';
import { MatInputModule} from '@angular/material/input';
import { MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FilterSearchComponent } from './components/filter-search/filter-search.component';
import {MatChipsModule} from "@angular/material/chips";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatSelectModule} from '@angular/material/select';
import {ItemCardSvg} from "./components/item-card/item-card.svg";
import {QuickItemCardSvg} from "./components/quick-item-card/quick-item-card.svg";
import {AdsenseModule} from "ng2-adsense";
import { SelectOptionTextComponent } from './components/select-option-text/select-option-text.component';
import {MatMenuModule} from "@angular/material/menu";
import {MatBottomSheetModule} from "@angular/material/bottom-sheet";
import { RoundButtonComponent } from './components/round-button/round-button.component';
import { SelectionChipsComponent } from './components/selection-chips/selection-chips.component';
import { MsExpansionPanelComponent } from './components/ms-expansion-panel/ms-expansion-panel.component';
import {MatExpansionModule} from "@angular/material/expansion";




@NgModule({
  declarations: [
    FilterSearchComponent,
    SelectOptionTextComponent,
    RoundButtonComponent,
    SelectionChipsComponent,
    MsExpansionPanelComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatDividerModule,
    MatCardModule,
    MatSliderModule,
    MatListModule,
    MatSnackBarModule,
    MatProgressBarModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatSelectModule,
    ItemCardSvg,
    QuickItemCardSvg,
    AdsenseModule,
    MatMenuModule,
    NgOptimizedImage,
    MatExpansionModule
  ],
    exports: [
        MatDialogModule,
        MatDividerModule,
        MatCardModule,
        MatSliderModule,
        MatListModule,
        MatSnackBarModule,
        MatProgressBarModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatToolbarModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        MatChipsModule,
        MatAutocompleteModule,
        MatSelectModule,
        FilterSearchComponent,
        SelectOptionTextComponent,
        RoundButtonComponent,
        MatBottomSheetModule,
        SelectionChipsComponent,
        MsExpansionPanelComponent
    ]
})
export class SharedModule { }
