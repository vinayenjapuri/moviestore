import { NgModule } from '@angular/core';
import {Data, RouterModule, Routes} from '@angular/router';
import {AboutRootComponent} from "./dashboard/about-root/about-root.component";

const getData = (route: string): Data => {
  return {
    "path": route
  }
}

const routes: Routes = [
  { path: '', component: AboutRootComponent},
  { path: 'series', loadChildren: () => import('./series/series.module').then(m => m.SeriesModule), data: getData('series')},
  { path: 'movies', loadChildren: () => import('./movies/movies.module').then(m => m.MoviesModule), data: getData('movies')},
  { path: 'mangas', loadChildren: () => import('./mangas/mangas.module').then(m => m.MangasModule), data: getData('mangas')},
  { path: 'animes', loadChildren: () => import('./animes/animes.module').then(m => m.AnimesModule), data: getData('animes')}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
