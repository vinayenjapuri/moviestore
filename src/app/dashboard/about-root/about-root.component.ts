import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {CinematicDumpState} from "../../cinematic-dump-store/cd-state";
import {getMobileResolution} from "../../cinematic-dump-store/cd-selectors";
import {Subscription} from "rxjs";

@Component({
  selector: 'ms-about-root',
  templateUrl: './about-root.component.html',
  styleUrls: ['./about-root.component.scss']
})
export class AboutRootComponent implements OnInit, OnDestroy {

  public isMobileWidth: boolean = false;
  private mobileResolutionSubscription$?: Subscription;

  constructor(private store: Store<CinematicDumpState>) {
  }
  ngOnInit(): void {
    this.subscribeToWindowResize();
  }

  private subscribeToWindowResize() {
    this.mobileResolutionSubscription$ = this.store.select(getMobileResolution).subscribe((isMobileRes ) => {
      this.isMobileWidth = <boolean>isMobileRes;
    })
  }

  ngOnDestroy(): void {
    this.mobileResolutionSubscription$?.unsubscribe();
  }
}
