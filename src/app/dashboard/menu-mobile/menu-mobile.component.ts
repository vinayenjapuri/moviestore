import {Component, Inject} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from "@angular/material/bottom-sheet";
import {CINEMATIC_DUMP_MODE_TYPES} from "../../shared/enums/cinematic-dump-mode-types";

@Component({
  selector: 'ms-menu-mobile',
  templateUrl: './menu-mobile.component.html',
  styleUrls: ['./menu-mobile.component.scss']
})
export class MenuMobileComponent {

  public modes = CINEMATIC_DUMP_MODE_TYPES;

  constructor( @Inject(MAT_BOTTOM_SHEET_DATA) public data: any, private matBottomSheetRef: MatBottomSheetRef) {
  }


  openClick($event: CINEMATIC_DUMP_MODE_TYPES) {
    this.matBottomSheetRef.dismiss($event);
  }

  closeSheet() {
    this.matBottomSheetRef.dismiss();
  }
}
