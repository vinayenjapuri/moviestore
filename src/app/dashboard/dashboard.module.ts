import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from "../shared/shared.module";
import {HomeComponent} from "./home/home.component";
import {StoreModule} from "@ngrx/store";
import {cinematicDumpReducer} from "../cinematic-dump-store/cd-reducers";
import { AboutRootComponent } from './about-root/about-root.component';
import {AboutMobileSvg} from "../shared/components/about-mobile/about-mobile.svg";
import {AboutSvg} from "../shared/components/about/about.svg";
import { MenuMobileComponent } from './menu-mobile/menu-mobile.component';
import {MatTooltipModule} from "@angular/material/tooltip";



@NgModule({
  declarations: [
    HomeComponent,
    AboutRootComponent,
    MenuMobileComponent
  ],
    imports: [
        CommonModule,
        SharedModule,
        StoreModule.forFeature('cinematic-dumpState', cinematicDumpReducer),
        AboutMobileSvg,
        AboutSvg,
        MatTooltipModule
    ],
  exports: [
    HomeComponent
  ]
})
export class DashboardModule { }
