import {Component, OnDestroy, OnInit} from '@angular/core';
import {CINEMATIC_DUMP_MODE_TYPES} from '../../shared/enums/cinematic-dump-mode-types';
import {Store} from '@ngrx/store';
import {CinematicDumpState} from '../../cinematic-dump-store/cd-state';
import {getCurrentMode, getMobileResolution} from '../../cinematic-dump-store/cd-selectors';
import {setMode} from 'src/app/cinematic-dump-store/cd-action';
import {Router} from "@angular/router";
import {MatBottomSheet} from "@angular/material/bottom-sheet";
import {MenuMobileComponent} from "../menu-mobile/menu-mobile.component";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  public currentMode?: CINEMATIC_DUMP_MODE_TYPES;
  public modes = CINEMATIC_DUMP_MODE_TYPES;
  public isMobileWidth: boolean = false;
  private bottomSheetSubscription$?: Subscription;
  private currentModeSubscription$?: Subscription;
  private mobileResSubscription$?: Subscription;

  constructor(private store: Store<CinematicDumpState>, private router: Router, private matBottomSheet: MatBottomSheet) {}

  ngOnInit(): void {
    this.subscribeToWindowResize();
    this.subscribeToCurrentMode();
  }

  subscribeToCurrentMode(): void {
    this.currentModeSubscription$ = this.store.select(getCurrentMode).subscribe((mode) => {
      this.currentMode = mode;
    });
  }

  setMode(mode: CINEMATIC_DUMP_MODE_TYPES): void {
    this.currentMode = mode === this.currentMode ? CINEMATIC_DUMP_MODE_TYPES.NONE : mode;
    const route = this.currentMode.toString().toLowerCase()
    this.router.navigate([`/${ route=== 'none'? '': route}`]).then(res => {
      if (res) {
        this.store.dispatch(setMode(this.currentMode));
      } else {
        this.currentMode = CINEMATIC_DUMP_MODE_TYPES.NONE;
      }
    })
  }


  private subscribeToWindowResize() {
    this.mobileResSubscription$ = this.store.select(getMobileResolution).subscribe((isMobileRes ) => {
      this.isMobileWidth = <boolean>isMobileRes;
    })
  }

  openMenu() {
    let bottomSheetRef = this.matBottomSheet.open(MenuMobileComponent, {
      hasBackdrop: true,
      disableClose: true,
      data: {
        mode: this.currentMode
      },
      ariaModal: true,
      ariaLabel: 'Select an option',
      direction: "ltr",
      panelClass: 'menu--mobile'
    });

    this.bottomSheetSubscription$ = bottomSheetRef.afterDismissed().subscribe(mode => {
      mode && this.setMode(mode);
    })
  }

  ngOnDestroy(): void {
    this.currentModeSubscription$?.unsubscribe();
    this.bottomSheetSubscription$?.unsubscribe();
    this.mobileResSubscription$?.unsubscribe();
  }
}
