import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeriesRootComponent } from './series-root.component';

describe('SeriesRootComponent', () => {
  let component: SeriesRootComponent;
  let fixture: ComponentFixture<SeriesRootComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SeriesRootComponent]
    });
    fixture = TestBed.createComponent(SeriesRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
