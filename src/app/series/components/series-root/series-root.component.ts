import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {CinematicDumpState} from "../../../cinematic-dump-store/cd-state";
import {SeriesService} from "../../series.service";
import {AbstractRootControl} from "../../../shared/abstract-classes/abstract-root-control";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'ms-series-root',
  templateUrl: './series-root.component.html',
  styleUrls: ['./series-root.component.scss']
})
export class SeriesRootComponent extends AbstractRootControl implements OnInit, OnDestroy {
  constructor(private seriesService: SeriesService, private movieStore: Store<CinematicDumpState>, private route: ActivatedRoute) {
    super(movieStore, seriesService, route)
  }

  ngOnDestroy(): void {
    this.dataSubscription?.unsubscribe();
    this.cinematicDumpAggregateSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.registerConfigurationImplementation().then(() => {

    });
  }
}
