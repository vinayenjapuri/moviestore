import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SeriesRoutingModule } from './series-routing.module';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { cinematicDumpReducer } from '../cinematic-dump-store/cd-reducers';
import { SeriesRootComponent } from './components/series-root/series-root.component';
import {ItemsContainerComponent} from "../shared/components/items-container/items-container.component";
import {ItemDetailedContentSvg} from "../shared/components/item-detailed-content/item-detailed-content.svg";
import {LoadingComponent} from "../shared/components/loading/loading.component";



@NgModule({
  declarations: [
    SeriesRootComponent
  ],
    imports: [
        CommonModule,
        SeriesRoutingModule,
        SharedModule,
        ItemsContainerComponent,
        StoreModule.forFeature('cinematic-dumpState', cinematicDumpReducer),
        ItemDetailedContentSvg,
        LoadingComponent
    ]
})
export class SeriesModule { }
