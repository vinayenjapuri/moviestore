import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable, map, catchError} from 'rxjs';
import { environment } from '../../environments/environment';
import {CinematicDumpAggregate, ItemAggregate} from '../shared/models/cinematic-dump.model';

@Injectable({
  providedIn: 'root'
})
export class SeriesService {

  constructor(private httpClient: HttpClient) { }

  getAggregate(): Observable<CinematicDumpAggregate[]> {
    return this.httpClient.get<ItemAggregate[]>(`${environment.webServiceUrl}/${environment.series_aggregate}`).pipe(
      map(items => {
        const cinematicDumpAggregate: CinematicDumpAggregate[] = [];
        items.forEach(item => {
          cinematicDumpAggregate.push(new CinematicDumpAggregate(item));
        });
        return cinematicDumpAggregate;
      }), catchError((err, caught) => {
        return [];
      })
    );
  }


}
