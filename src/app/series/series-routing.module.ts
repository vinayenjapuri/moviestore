import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SeriesRootComponent} from "./components/series-root/series-root.component";

const seriesRoutes: Routes = [
  {
    path: '',
    component: SeriesRootComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(seriesRoutes)],
  exports: [RouterModule]
})
export class SeriesRoutingModule { }
